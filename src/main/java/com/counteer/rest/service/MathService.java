package com.counteer.rest.service;

import org.springframework.stereotype.Service;

import com.counteer.rest.annotation.RandomLog;

@Service
public class MathService {

	@RandomLog
	public int add(int i, int j) {
		return i + j;
	}

}
