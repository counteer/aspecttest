package com.counteer.rest.service;

import static java.util.UUID.randomUUID;

import org.springframework.stereotype.Service;

@Service
public class RandomGeneratorService {

	public String generateUUID() {
		return randomUUID().toString();
	}
}
