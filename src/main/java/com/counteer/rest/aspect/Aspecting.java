package com.counteer.rest.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.counteer.rest.service.RandomGeneratorService;

@Component
@Aspect
public class Aspecting {

	@Autowired
	private RandomGeneratorService service;
	
    @After(value = "@annotation(com.counteer.rest.annotation.RandomLog)")
    public void after(JoinPoint joinPoint) throws Throwable {
    	System.out.println(service.generateUUID());
    }

}
