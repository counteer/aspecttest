package com.counteer.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy
public class WeavingApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeavingApplication.class, args);
	}
}
